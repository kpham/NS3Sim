
#include "ns3/buildings-module.h"
#include "ns3/mobility-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include <fstream>
#include "ns3/uniform-planar-array.h"
#include "ns3/three-gpp-spectrum-propagation-loss-model.h"
#include "ns3/three-gpp-v2v-propagation-loss-model.h"
#include "ns3/three-gpp-channel-model.h"
#include "ns3/netanim-module.h"
#include <math.h>
#include <ns3/lte-module.h>
#include <ns3/constant-velocity-mobility-model.h>
#include <ns3/constant-velocity-helper.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/ipv4-routing-helper.h>
#include <ns3/ipv4-static-routing-helper.h>
#include <ns3/internet-stack-helper.h>

// this file is based off of getting my gpp file and adding LTE functionality to it
// as opposed to adding GPP to my LTE
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ThreeGppV2vChannelExample");

static Ptr<ThreeGppPropagationLossModel> m_propagationLossModel; //!< the PropagationLossModel object
static Ptr<ThreeGppSpectrumPropagationLossModel> m_spectrumLossModel; //!< the SpectrumPropagationLossModel object
static Ptr<ChannelConditionModel> m_condModel; //!< the ChannelConditionModel object

/**
 * Perform the beamforming using the DFT beamforming method
 * \param thisDevice the device performing the beamforming
 * \param thisAntenna the antenna object associated to thisDevice
 * \param otherDevice the device towards which point the beam
 */
static void
DoBeamforming (Ptr<NetDevice> thisDevice, Ptr<PhasedArrayModel> thisAntenna, Ptr<NetDevice> otherDevice)
{
  PhasedArrayModel::ComplexVector antennaWeights;

  // retrieve the position of the two devices
  Vector aPos = thisDevice->GetNode ()->GetObject<MobilityModel> ()->GetPosition ();
  Vector bPos = otherDevice->GetNode ()->GetObject<MobilityModel> ()->GetPosition ();

  // compute the azimuth and the elevation angles
  Angles completeAngle (bPos,aPos);

  PhasedArrayModel::ComplexVector bf = thisAntenna->GetBeamformingVector (completeAngle);
  thisAntenna->SetBeamformingVector (bf);
}

// finds the distance between the transmitter and reciever
static double
findDist(Ptr<MobilityModel> txMob, Ptr<MobilityModel> rxMob){
  return std::abs(sqrt(
    (pow((txMob->GetPosition().x - rxMob->GetPosition().x),2) + 
    pow((txMob->GetPosition().y - rxMob->GetPosition().y),2))));
}

/**
 * Compute the average SNR
 * \param txMob the tx mobility model
 * \param rxMob the rx mobility model
 * \param txPsd the PSD of the transmitting signal
 * \param noiseFigure the noise figure in dB
 */
static void
ComputeSnr (Ptr<MobilityModel> txMob, Ptr<MobilityModel> rxMob, Ptr<const SpectrumValue> txPsd, double noiseFigure)
{
  Ptr<SpectrumValue> rxPsd = txPsd->Copy ();

  // check the channel condition
  Ptr<ChannelCondition> cond = m_condModel->GetChannelCondition (txMob, rxMob);

  // apply the pathloss
  double propagationGainDb = m_propagationLossModel->CalcRxPower (0, txMob, rxMob);
  NS_LOG_DEBUG ("Pathloss " << -propagationGainDb << " dB");
  double propagationGainLinear = std::pow (10.0, (propagationGainDb) / 10.0);
  *(rxPsd) *= propagationGainLinear;

  // apply the fast fading and the beamforming gain
  rxPsd = m_spectrumLossModel->CalcRxPowerSpectralDensity (rxPsd, txMob, rxMob);
  NS_LOG_DEBUG ("Average rx power " << 10 * log10 (Sum (*rxPsd) * 180e3) << " dB");

  // create the noise psd
  // taken from lte-spectrum-value-helper
  const double kT_dBm_Hz = -174.0; // dBm/Hz
  double kT_W_Hz = std::pow (10.0, (kT_dBm_Hz - 30) / 10.0);
  double noiseFigureLinear = std::pow (10.0, noiseFigure / 10.0);
  double noisePowerSpectralDensity =  kT_W_Hz * noiseFigureLinear;
  Ptr<SpectrumValue> noisePsd = Create <SpectrumValue> (txPsd->GetSpectrumModel ());
  (*noisePsd) = noisePowerSpectralDensity;

  // compute the SNR
  NS_LOG_DEBUG ("Average SNR " << 10 * log10 (Sum (*rxPsd) / Sum (*noisePsd)) << " dB");

  // print the SNR and pathloss values in the snr-trace.txt file
  std::ofstream f;
  f.open ("LteGppSnr.txt", std::ios::out | std::ios::app);
  f << Simulator::Now ().GetSeconds () << " " // time [s]
    << findDist(txMob,rxMob) << " " // distance formula
    << 10 * log10 (Sum (*rxPsd) / Sum (*noisePsd)) << " " // SNR [dB]
    << -propagationGainDb << std::endl; // pathloss [dB]
  f.close ();
} 

int
main (int argc, char *argv[])
{
  double frequency = 28.0e9; // operating frequency in Hz
  double txPow_dbm = 30.0; // tx power in dBm
  double noiseFigure = 9.0; // noise figure in dB
  Time simTime = Seconds (40); // simulation time
  Time timeRes = MilliSeconds (10); // time resolution
  std::string scenario = "V2V-Urban"; // 3GPP propagation scenario, V2V-Urban or V2V-Highway
  double vScatt = 0; // maximum speed of the vehicles in the scenario [m/s]
  double subCarrierSpacing = 60e3; // subcarrier spacing in kHz
  uint32_t numRb = 275; // number of resource blocks

  std::ofstream f;
  f.open("LteGppSnr.txt", std::ofstream::out | std::ofstream::trunc);
  f.close();
  // this line wipes the file so it wont keep writing on top of old data.

  CommandLine cmd (__FILE__);
  cmd.AddValue ("frequency", "operating frequency in Hz", frequency);
  cmd.AddValue ("txPow", "tx power in dBm", txPow_dbm);
  cmd.AddValue ("noiseFigure", "noise figure in dB", noiseFigure);
  cmd.AddValue ("scenario", "3GPP propagation scenario, V2V-Urban or V2V-Highway", scenario);
  cmd.Parse (argc, argv);

  // create the antennas
  NodeContainer antennas;
  antennas.Create (2);

    // create the tx and rx devices
  Ptr<SimpleNetDevice> txDev = CreateObject<SimpleNetDevice> ();
  Ptr<SimpleNetDevice> rxDev = CreateObject<SimpleNetDevice> ();

  // associate the nodes and the devices
  antennas.Get (0)->AddDevice (txDev);
  txDev->SetNode (antennas.Get (0));
  antennas.Get (1)->AddDevice (rxDev);
  rxDev->SetNode (antennas.Get (1));

  // create the antenna objects and set their dimensions
  Ptr<PhasedArrayModel> txAntenna = CreateObjectWithAttributes<UniformPlanarArray> ("NumColumns", UintegerValue (2), "NumRows", UintegerValue (2), "BearingAngle", DoubleValue (-M_PI / 2));
  Ptr<PhasedArrayModel> rxAntenna = CreateObjectWithAttributes<UniformPlanarArray> ("NumColumns", UintegerValue (2), "NumRows", UintegerValue (2), "BearingAngle", DoubleValue (M_PI / 2));

  Ptr<MobilityModel> txMob;
  Ptr<MobilityModel> rxMob;

  //create LTE object to help install 
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();

  //create EPC object to help install
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper>();
  
  //connect the two
  lteHelper->SetEpcHelper(epcHelper);

  //Get the pgw and sgw nodes from the epc helper
  Ptr<Node> pgw = epcHelper->GetPgwNode();
  Ptr<Node> sgw = epcHelper->GetSgwNode();
  Ptr<Node> mme = NodeList::GetNode(4);
  // I am not sure how to access this node otherwise?

  // Create nodes for the enb, ue and remoteHost
  NodeContainer enbNodes,ueNodes,remoteHost;
  remoteHost.Create(1);
  enbNodes.Create(1);
  ueNodes.Create(1);


  // install internet protocol for remoteHost
  InternetStackHelper internet;
  internet.Install(remoteHost);
  internet.Install(ueNodes);

  if (scenario == "V2V-Urban")
    {
      // set the mobility models for every node
      Ptr<MobilityModel> txMob;
      txMob = CreateObject<ConstantVelocityMobilityModel> ();
      Vector txVector (45,60,0);
      txMob->GetObject<ConstantVelocityMobilityModel>()->SetPosition(txVector);
      txMob->GetObject<ConstantVelocityMobilityModel>()->SetVelocity(Vector(5,5,0));
      antennas.Get (0)->AggregateObject (txMob);

      Ptr<MobilityModel> ueMob;
      ueMob = CreateObject<ConstantVelocityMobilityModel> ();
      Vector ueVector (45,60,0);
      ueMob->GetObject<ConstantVelocityMobilityModel>()->SetPosition(ueVector);
      ueMob->GetObject<ConstantVelocityMobilityModel>()->SetVelocity(Vector(5,5,0));
      enbNodes.Get (0)->AggregateObject (ueMob);


      Ptr<MobilityModel> rxMob;
      rxMob = CreateObject<ConstantVelocityMobilityModel> ();
      Vector rxVector (20,60,0);
      rxMob->GetObject<ConstantVelocityMobilityModel>()->SetPosition(rxVector);
      rxMob->GetObject<ConstantVelocityMobilityModel>()->SetVelocity(Vector(5,5,0));
      antennas.Get (1)->AggregateObject (rxMob);

      Ptr<MobilityModel> enbMob;
      enbMob = CreateObject<ConstantVelocityMobilityModel> ();
      Vector enbVector (20,60,0);
      enbMob->GetObject<ConstantVelocityMobilityModel>()->SetPosition(enbVector);
      enbMob->GetObject<ConstantVelocityMobilityModel>()->SetVelocity(Vector(5,5,0));
      ueNodes.Get (0)->AggregateObject (enbMob);
            

      Ptr<MobilityModel> pgwMob;
      pgwMob = CreateObject<ConstantPositionMobilityModel>();
      Vector pgwVector(40,100,0);
      pgwMob->SetPosition(pgwVector);
      pgw->AggregateObject(pgwMob);

      Ptr<MobilityModel> sgwMob;
      sgwMob = CreateObject<ConstantPositionMobilityModel>();
      Vector sgwVector(40,120,0);
      sgwMob->SetPosition(sgwVector);
      sgw->AggregateObject(sgwMob);

      Ptr<MobilityModel> mmeMob;
      mmeMob = CreateObject<ConstantPositionMobilityModel>();
      Vector mmeVector(40,140,0);
      mmeMob->SetPosition(mmeVector);
      mme->AggregateObject(mmeMob);

      Ptr<MobilityModel> rHMob;
      rHMob = CreateObject<ConstantPositionMobilityModel>();
      Vector rHVector(40,80,0);
      rHMob->SetPosition(rHVector);
      remoteHost.Get(0)->AggregateObject(rHMob);

      // create the channel condition model
      m_condModel = CreateObject<ThreeGppV2vUrbanChannelConditionModel> ();

      // create the propagation loss model
      m_propagationLossModel = CreateObject<ThreeGppV2vUrbanPropagationLossModel> ();
    }

  NetDeviceContainer enbDevices,ueDevices;
  enbDevices = lteHelper->InstallEnbDevice(enbNodes);
  ueDevices = lteHelper->InstallUeDevice(ueNodes);
  
    PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("10Gb/s")));
  p2p.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2p.SetChannelAttribute ("Delay", TimeValue (Seconds (.010)));
  
  // connect the PGW to RemoteHost
  NetDeviceContainer internetDevices = p2p.Install(pgw,remoteHost.Get(0));
  // assigning the IP address
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4.Assign (internetDevices);


  // static routing because we aren't moving and changing address, no dynamic IP here
  Ipv4StaticRoutingHelper Ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting;
  
  // set the address of remoteHostStaticRouting to the static Ipv4 address of remoteHost
  remoteHostStaticRouting = Ipv4RoutingHelper.GetStaticRouting(remoteHost.Get(0)->GetObject<Ipv4>());
  
  // The line doesnt match the tutorial? and was told to do this instead
  // https://www.nsnam.org/bugzilla/show_bug.cgi?id=2999
  remoteHostStaticRouting ->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.0.0.0"),1);


  Ipv4InterfaceContainer ueIpIface;
  Ptr<Node> ue = ueNodes.Get (0);
  Ptr<NetDevice> ueLteDevice = ueDevices.Get (0);
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevice));
  //assign an IP adress to a ueDevice
  // set the default gateway for the UE
  Ptr<Ipv4StaticRouting> ueStaticRouting;
  ueStaticRouting = Ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
  ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
  //set a route to it
    
  m_condModel->SetAttribute ("UpdatePeriod", TimeValue (MilliSeconds (100)));

  m_propagationLossModel->SetAttribute ("Frequency", DoubleValue (frequency));
  m_propagationLossModel->SetAttribute ("ShadowingEnabled", BooleanValue (false));
  m_propagationLossModel->SetAttribute ("ChannelConditionModel", PointerValue (m_condModel));

  // create the channel model
  Ptr<ThreeGppChannelModel> channelModel = CreateObject<ThreeGppChannelModel> ();
  channelModel->SetAttribute ("Scenario", StringValue (scenario));
  channelModel->SetAttribute ("Frequency", DoubleValue (frequency));
  channelModel->SetAttribute ("ChannelConditionModel", PointerValue (m_condModel));

  // create the spectrum propagation loss model
  m_spectrumLossModel = CreateObjectWithAttributes<ThreeGppSpectrumPropagationLossModel> ("ChannelModel", PointerValue (channelModel));
  m_spectrumLossModel->SetAttribute ("vScatt", DoubleValue (vScatt));

  // initialize the devices in the ThreeGppSpectrumPropagationLossModel
  m_spectrumLossModel->AddDevice (txDev, txAntenna);
  m_spectrumLossModel->AddDevice (rxDev, rxAntenna);

  BuildingsHelper::Install (antennas);


  // set the beamforming vectors
  DoBeamforming (txDev, txAntenna, rxDev);
  DoBeamforming (rxDev, rxAntenna, txDev);

  // create the tx power spectral density
  Bands rbs;
  double freqSubBand = frequency;
  for (uint16_t n = 0; n < numRb; ++n)
    {
      BandInfo rb;
      rb.fl = freqSubBand;
      freqSubBand += subCarrierSpacing / 2;
      rb.fc = freqSubBand;
      freqSubBand += subCarrierSpacing / 2;
      rb.fh = freqSubBand;
      rbs.push_back (rb);
    }
  Ptr<SpectrumModel> spectrumModel = Create<SpectrumModel> (rbs);
  Ptr<SpectrumValue> txPsd = Create <SpectrumValue> (spectrumModel);
  double txPow_w = std::pow (10., (txPow_dbm - 30) / 10);
  double txPowDens = (txPow_w / (numRb * subCarrierSpacing));
  (*txPsd) = txPowDens;

  for (int i = 0; i < simTime / timeRes; i++)
    {
      Simulator::Schedule (timeRes * i, &ComputeSnr, txMob, rxMob, txPsd, noiseFigure);
    }


  AnimationInterface anim ("lteGPPdebug.xml");
  anim.UpdateNodeDescription(0,"txAntenna");
  anim.UpdateNodeDescription(1,"rxAntenna");
  anim.UpdateNodeDescription(2,"pgw");
  anim.UpdateNodeDescription(3,"sgw");
  anim.UpdateNodeDescription(4,"mme");
  anim.UpdateNodeDescription(5,"remoteHost");



  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
