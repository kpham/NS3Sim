This repository contains my scratch files.

All files were built on a macOs running Catalina 10.15.7, using NS3.33

The main simulator is in the LTE folder. It has many commands that can be changed by the user. To see them, run

**./waf --run "lte --help"**

To view the XML output files, you need to install NetAnim. Make sure you have sudo access.
