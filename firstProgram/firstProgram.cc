#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"

// planned topology
//
//       10.1.1.0
//  n0  n1  n2  n3  n4
//  ===================
//          csma
// n0 is the server, each node will ping it



using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Kevin's First Program :)");

int 
main (int argc, char *argv[])
{
  LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
  //output in console
  NodeContainer csmaNodes;
  csmaNodes.Create(5);
  // create  5  nodes

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
  //settings for csma connections

  NetDeviceContainer csmaDevices;
  csmaDevices = csma.Install (csmaNodes);
  // all of the csma nodes now have a device, using the settings defined above

  InternetStackHelper stack;
  stack.Install (csmaNodes);
  // all are using IP/TCP/UDP rules

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterface;
  csmaInterface = address.Assign (csmaDevices);
  // each device will have an IP starting with 10.1.1.0 , 10.1.1.1, 10.1.1.2, and so on

  // create a UDP echo server on port 9
  UdpEchoServerHelper echoServer (9);

  // server will be installed on node 0
  ApplicationContainer serverApps = echoServer.Install (csmaNodes.Get (0));
  serverApps.Start (Seconds (1.0));
  serverApps.Stop (Seconds (10.0));
  

  // echo client will send an address of 1 to port 9
  UdpEchoClientHelper echoClient (csmaInterface.GetAddress (0), 9);
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

  //assign node 1 to send something from 2-3 seconds
  ApplicationContainer clientApps = echoClient.Install (csmaNodes.Get (1));
  clientApps.Start (Seconds (2.0));
  clientApps.Stop (Seconds (3.0));

  //assign node 2 to send something from 3-4 seconds
  ApplicationContainer clientApps2 = echoClient.Install (csmaNodes.Get (2));
  clientApps2.Start (Seconds (3.0));
  clientApps2.Stop (Seconds (4.0));

    //assign node 3 to send something from 4-5 seconds
  ApplicationContainer clientApps3 = echoClient.Install (csmaNodes.Get (3));
  clientApps3.Start (Seconds (4.0));
  clientApps3.Stop (Seconds (5.0));

    //assign node 4 to send something from 5-6 seconds
  ApplicationContainer clientApps4 = echoClient.Install (csmaNodes.Get (4));
  clientApps4.Start (Seconds (5.0));
  clientApps4.Stop (Seconds (6.0));
 
  csma.EnablePcapAll ("test");
  csma.EnablePcap ("test", csmaDevices.Get (0), true);

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
