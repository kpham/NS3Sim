import matplotlib.pyplot as plt
import numpy as np


# scrape data from the output file
file = open('gppSNR.txt','r')
time0 = []
dist0 = []
snr0 = []
pathloss0 = []
for row in file:
    row = row.split(' ')
    time0.append(row[0])
    dist0.append(row[1])
    snr0.append(row[2])
    pathloss0.append(row[3])

# converts them into numpy arrays which have more functions

time = np.array(time0)
dist = np.array(dist0)
snr = np.array(snr0)
pathloss = np.array(pathloss0)


# first figure, pathloss vs distance
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()

x1 = np.array(time)
y1 = np.array(pathloss)
y2 = np.array(dist)


ax1.plot(time,pathloss, label="pathloss", color = 'g')
ax2.plot(time,dist, label="distance between nodes (m)", color = 'b')

ax1.set_xlabel('time (s)')
ax1.set_ylabel('pathloss (db)',color = 'g')
ax2.set_ylabel('distance (m)',color = 'b')

# second figure, signal to noise ration vs distance
fig2 = plt.figure()
ax3 = fig2.add_subplot(111)
ax4 = ax3.twinx()
ax3.plot(time,snr, label="snr", color = 'g')
ax4.plot(time,dist, label="distance between nodes (m)", color = 'b')

ax3.set_xlabel('time (s)')
ax3.set_ylabel('snr (db)',color = 'g')
ax4.set_ylabel('distance (m)',color = 'b')






plt.show()
