from os import path
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import UnivariateSpline




# scrape data from the output file
file = open('gppSNR.txt','r')
time0 = []
dist0 = []
snr0 = []
pathloss0 = []
for row in file:
    row = row.split(' ')
    time0.append(row[0])
    dist0.append(row[1])
    snr0.append(row[2])
    pathloss0.append(row[3])

# converts them into numpy arrays with floats 

time = np.array(time0).astype(np.float64)
dist = np.array(dist0).astype(np.float64)
snr = np.array(snr0).astype(np.float64)
pathloss = np.array(pathloss0).astype(np.float64)

#figure 1, snr & distance vs time 

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()

timeSmoothed = np.linspace(time.min(),time.max(),100)
snrSpline = UnivariateSpline(time,snr)
snrSmoothed = snrSpline(timeSmoothed)
distSpline = UnivariateSpline(time,dist)
distSmoothed = distSpline(timeSmoothed)

ax1.set_title('SNR vs Distance')
ax1.set_xlabel('time (s)')
ax2.set_ylabel('snr (db)', color = 'g')
ax1.set_ylabel('distance (m)', color = 'b')
ax2.plot(timeSmoothed,snrSmoothed,color = 'g')
ax1.plot(timeSmoothed,distSmoothed,color = 'b')

#figure 2, pathloss & distance vs time

fig2 = plt.figure()
ax3 = fig2.add_subplot(111)
ax4 = ax3.twinx()

pathlossSpline = UnivariateSpline(time,pathloss)
pathLossSmoothed = pathlossSpline(timeSmoothed)

ax3.set_title('Pathloss vs Distance')
ax3.set_xlabel('time (s)')
ax4.set_ylabel('pathloss (db)', color = 'g')
ax3.set_ylabel('distance (m)', color = 'b')
ax4.plot(timeSmoothed,pathLossSmoothed,color = 'g',label = 'pathloss')
ax3.plot(timeSmoothed,distSmoothed,color = 'b', label = 'distance')










plt.show()
