#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/lte-module.h>
#include <ns3/config-store.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/ipv4-static-routing-helper.h>
#include <ns3/ipv4-routing-helper.h>
#include <ns3/epc-helper.h>
#include <ns3/udp-client-server-helper.h>
#include <ns3/packet-sink-helper.h>
#include <ns3/netanim-module.h>
#include <ns3/udp-echo-helper.h>
#include <ns3/propagation-loss-model.h>
#include "ns3/buildings-module.h"

#include "ns3/random-waypoint-mobility-model.h"
#include "ns3/random-walk-2d-mobility-model.h"


#include "ns3/flow-monitor-module.h"


using namespace ns3;

/*
* This program is from following information on the https://www.nsnam.org/docs/models/html/lte-user.html
* To use the default input.txt LTE settings, please run
* ./waf --command-template="%s --ns3::ConfigStore::Filename=input.txt --ns3::ConfigStore::Mode=Load --ns3::ConfigStore::FileFormat=RawText" --run lte
*
* This program will create an LTE network with an EPC, 2 ENB's and 2 UE's. 
* 
*
* launch options
* ./waf --run "lte --flag=[value] --flag2=[value]"
* ./waf --run "lte --help" to print out all the variables that can be changed
* A note that the images for netanim use absolute path, and will need to be adjusted. It may just need the user name changed  
*/


int main (int argc, char *argv[])
{
  int modeArg =1;
  int mobilityArg = 0;
  int debugArg = 0;
  int flowArg = 0;
  int ueDist = 100;
  int echoPackSize = 1024;
  double echoPackTime = 1;
  int echoPackAmt = 5000;
  int ueNodesAmt = 2;
  int simTime = 20;
  int rho = 50;
  int enbNodesAmt = 2;
  double enbRange = 300;
  double initTime = 1;

  uint32_t SentPackets = 0;
  uint32_t LostPackets = 0;
  uint32_t ReceivedPackets = 0;

  std::ofstream f;
  f.open("lteFlowOutput.txt", std::ofstream::out | std::ofstream::trunc);
  f.close();
  // this line wipes the file so it wont keep writing on top of old data.

  CommandLine cmd (__FILE__);
  cmd.AddValue("mode", "choose echo or downstream" , modeArg);
  cmd.AddValue("mobility", "choose stationary UE or moving" , mobilityArg);
  cmd.AddValue("debug", "print debug info", debugArg);
  cmd.AddValue("flow","print flowmonitor info",flowArg);
  cmd.AddValue("echoPackSize", "size in bytes of each packet for UE's to send to echoserver", echoPackSize);
  cmd.AddValue("echoPackTime","time duration in seconds for the packets to be sent",echoPackTime);
  cmd.AddValue("echoPackAmt","the max amount of packets to send",echoPackAmt);
  cmd.AddValue("ueNodes","How many UE Nodes in the simulation",ueNodesAmt);
  cmd.AddValue("ueDist","set the distance in meters of the UE's. Will spawn near coordinate",ueDist);
  cmd.AddValue("enbNodes","How many ENB Nodes in the simulation",enbNodesAmt);
  cmd.AddValue("enbRange","Space the nodes out over this range",enbRange);
  cmd.AddValue("simTime","The simulation time in seconds",simTime);
  cmd.AddValue("spread","the radius that UE's will spread across",rho);
  cmd.AddValue("initTime","The time spacing for each node to begin sending",initTime);
  cmd.Parse (argc, argv);  

  // This prints some terminal output. RRC is the radio Resource Control that connects the network layer of LTE
  if (debugArg == 1){
    LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
    LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
   //LogComponentEnable ("LteEnbRrc", LOG_LEVEL_INFO);
   //may be useful for some but I couldnt find a use for it
  }


  //create LTE object to help install 
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();

  //create EPC object to help install
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper>();
  
  //connect the two
  lteHelper->SetEpcHelper(epcHelper);

  //create pgw node
  Ptr<Node> pgw = epcHelper->GetPgwNode();
  Ptr<Node> sgw = epcHelper->GetSgwNode();
  // im not sure where to find MME but it is node 2 in netAnim
  Ptr<Node> mme = NodeList::GetNode(2);

  //create nodes for the eNB, UE and remoteHost
  NodeContainer enbNodes,ueNodes,remoteHostCont;
  remoteHostCont.Create(1);
  enbNodes.Create(enbNodesAmt);
  ueNodes.Create(ueNodesAmt);

  // a pointer to the first node in the remoteHostCont container  
  Ptr<Node> remoteHost = remoteHostCont.Get(0);

   // install internet functionality onto remoteHost and the UEnodes
   InternetStackHelper internet;
    internet.Install(remoteHostCont);
    internet.Install(ueNodes);
  
  MobilityHelper mobility;
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisPropagationLossModel"));
  // place enbNodes

  int enbNodesAmtFixed = enbNodesAmt-1; 
  double enbStepSize = enbRange/enbNodesAmtFixed;

  for (int c = 0; c<enbNodesAmt;c++){
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                              "MinX", DoubleValue(c*enbStepSize),
                              "MinY", DoubleValue(60));
  mobility.Install(enbNodes.Get(c)); 
  }

  
  //place remoteHost
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                              "MinX", DoubleValue(150),
                              "MinY", DoubleValue(0));
  mobility.Install(remoteHostCont);

  //place pgw 
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                              "MinX", DoubleValue(150),
                              "MinY", DoubleValue(15));
  mobility.Install(pgw);

  //place sgw 
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                              "MinX", DoubleValue(160),
                              "MinY", DoubleValue(40));
  mobility.Install(sgw);

    //place mme
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                              "MinX", DoubleValue(130),
                              "MinY", DoubleValue(40));
  mobility.Install(mme);

  //place ue nodes
  mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                   "X", DoubleValue (enbRange/2),
                                   "Y", DoubleValue (60+ueDist),
                                   "rho", DoubleValue (rho));

    // mobility.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
    //                                "X", StringValue ("ns3::UniformRandomVariable[Min=50.0|Max=300.0]"),
    //                                "Y", StringValue ("ns3::UniformRandomVariable[Min=85.0|Max=200.0]"));

  if (mobilityArg==1){
  mobility.SetMobilityModel ("ns3::RandomWalk2dOutdoorMobilityModel",
                                  "Speed", StringValue ("ns3::UniformRandomVariable[Min=100.0|Max=250.0]"),
                                  "Bounds", StringValue("0|10000|0|10000"),
                                  "Time", TimeValue(Seconds(simTime)));
  }
  mobility.Install (ueNodes);
  

  //create devices that will have the LTE stack installed on them
  NetDeviceContainer enbDevices,ueDevices;
  enbDevices = lteHelper->InstallEnbDevice(enbNodes);
  ueDevices = lteHelper->InstallUeDevice(ueNodes);

  // we want to create a point to point connection from the PGW to the remote host
  PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("10Gb/s")));
  p2p.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2p.SetChannelAttribute ("Delay", TimeValue (Seconds (.010)));
  // connect the PGW to RemoteHost
  NetDeviceContainer internetDevices = p2p.Install(pgw,remoteHost);
  
  // assigning the IP address
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4.Assign (internetDevices);
  

  // static routing because we aren't moving and changing address, no dynamic IP here
  Ipv4StaticRoutingHelper Ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting;
  
  // set the address of remoteHostStaticRouting to the static Ipv4 address of remoteHost
  remoteHostStaticRouting = Ipv4RoutingHelper.GetStaticRouting(remoteHost->GetObject<Ipv4>());
  
  // The line doesnt match the tutorial? and was told to do this instead
  // https://www.nsnam.org/bugzilla/show_bug.cgi?id=2999
  remoteHostStaticRouting ->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.0.0.0"),1);
  Ipv4InterfaceContainer ueIpIface;
  // assign IP address to UEs

  for (uint32_t u = 0; u < ueNodes.GetN (); ++u){
    Ptr<Node> ue = ueNodes.Get (u);
    Ptr<NetDevice> ueLteDevice = ueDevices.Get (u);
    ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevice));
    //assign an IP adress to a ueDevice
    // set the default gateway for the UE
    Ptr<Ipv4StaticRouting> ueStaticRouting;
    ueStaticRouting = Ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
    ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    //set a route to it
  }

  //Set up the Traffic Flow Template and decide the port #
  Ptr<EpcTft> tft = Create<EpcTft> ();
  EpcTft::PacketFilter pf;
  pf.localPortStart = 1234;
  tft->Add (pf);
  pf.localPortEnd = 1234;
  //create a dedicated connection between ueDevices and the PDN
  lteHelper->ActivateDedicatedEpsBearer (ueDevices, EpsBearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT), tft);
  //The method used below connects to the closest enb it can find
  lteHelper->Attach(ueDevices);

  
  // downstream
  if (modeArg==0){
    uint16_t dlPort = 1234;
      // a packet sink recieves and consumes data given a port and Address
    PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dlPort));
      // the endpoint for data, or the sink, will be the ueNodes
    ApplicationContainer serverApps = packetSinkHelper.Install (ueNodes);
    serverApps.Start (Seconds (0.01));
    UdpClientHelper client (ueIpIface.GetAddress (0), dlPort);
    ApplicationContainer clientApps = client.Install (remoteHost);
    clientApps.Start (Seconds (0.01));
  }

  // echo server
  if (modeArg==1){
      // create an echo server on port 9
    UdpEchoServerHelper echoServer (9);
      // packet gateway node is the echo server, and will respond when pinged
    ApplicationContainer serverApps = echoServer.Install(pgw);

      // get the address of the pgw, talk on port 9

    UdpEchoClientHelper echoClient(internetIpIfaces.GetAddress(0),9);
    echoClient.SetAttribute ("MaxPackets", UintegerValue (echoPackAmt));
    echoClient.SetAttribute ("Interval", TimeValue (Seconds (echoPackTime)));
    echoClient.SetAttribute ("PacketSize", UintegerValue (echoPackSize));

      // each UE node will send something

    
    // delay the start of packets
    for (int c = 0; c < ueNodesAmt; c++){
      ApplicationContainer clientApps = echoClient.Install(ueNodes.Get (c));
      
      clientApps.Start (Seconds (initTime));
      clientApps.Stop (Seconds (simTime));
      initTime+=2;
    }
  }

  // QoS class indicator. GBR_CONV_VOICE has highest priority service
  enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
  EpsBearer bearer (q);
  
 //netAnim stuff
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  AnimationInterface anim ("lte.xml");

  anim.SetMaxPktsPerTraceFile(99999999999999);
  //fixes that error when theres a lot of things to trace
  //anim.SetBackgroundImage("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/blankWhiteBackground.png",0,0,1,1,1);

  /* 
   * corresponding names in netAnim 
   * node 0 = pgw
   * node 1 = sgw
   * node 2 = mme
   * node 3 = enbNode0
   * node 4 = enbNode1
   * node 5 = ueNode0
   * node 6 = ueNode1
   * node 7 = remoteHost, or PDN
   */
  
  anim.UpdateNodeDescription(0,"pgw");
  anim.UpdateNodeDescription(1,"sgw");
  anim.UpdateNodeDescription(2,"mme");
  anim.UpdateNodeDescription(3,"remoteHost");
 
  // function description says it needs the absolute path, not relative, which is kinda lame.
  // change paths to your system
  // pgw node
  anim.AddResource("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/pgw.png");
  anim.UpdateNodeImage(0,0);
  anim.UpdateNodeSize(0,5,5);

  // sgw node
  anim.AddResource("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/sgw.png");
  anim.UpdateNodeImage(1,1);
  anim.UpdateNodeSize(1,5,5);

  // mme node
  anim.AddResource("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/map.png");
  anim.UpdateNodeImage(2,2);
  anim.UpdateNodeSize(2,5,5);
  
  // remote host
  anim.AddResource("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/remoteHost.png");
  anim.UpdateNodeImage(3,3);
  anim.UpdateNodeSize(3,5,5);

  // enbNodes
  int counter = 4;
  anim.AddResource("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/antenna.png");
  int stop = 4+enbNodesAmt;
  while (counter<stop){
    anim.UpdateNodeImage(counter,4);
    anim.UpdateNodeSize(counter,5,5);
    anim.UpdateNodeDescription(counter,"enbNode");
    counter++;
  }

  // ueNodes
   anim.AddResource("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/phone.png");
  stop += ueNodesAmt;
   while (counter<stop){
     anim.UpdateNodeImage(counter,5);
     anim.UpdateNodeSize(counter,5,5);
     //anim.UpdateNodeDescription(counter,"ueNode");
     counter++;   
   }

  

  //anim.SetBackgroundImage("/Users/khpham3/workspace/ns-3-allinone/ns-3-dev/scratch/lte/background.png",50,50,1,1,1);
  // used for testing but has been removed
  
  Simulator::Stop(Seconds(simTime));
  Simulator::Run ();
std::cout << " to view lte.xml output file, please open NetAnim";

  if (flowArg==1){
    // this code is provided by Technosilent, https://www.youtube.com/watch?v=3RhNXxb9FQA
    f.open ("lteFlowOutput.txt", std::ios::out | std::ios::app); 
    int j=0;
    float AvgThroughput = 0;
    Time Jitter;
    Time Delay;
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
    {
	  Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

  NS_LOG_UNCOND("----Flow ID:" <<iter->first);
  NS_LOG_UNCOND("Src Addr: " <<t.sourceAddress << std::endl << "Dst Addr: "<< t.destinationAddress);
  NS_LOG_UNCOND("Sent Packets=" <<iter->second.txPackets);
  NS_LOG_UNCOND("Received Packets =" <<iter->second.rxPackets);
  NS_LOG_UNCOND("Lost Packets =" <<iter->second.txPackets-iter->second.rxPackets);
  NS_LOG_UNCOND("Packet delivery ratio =" <<iter->second.rxPackets*100/iter->second.txPackets << "%");
  NS_LOG_UNCOND("Packet loss ratio =" << (iter->second.txPackets-iter->second.rxPackets)*100/iter->second.txPackets << "%");
  NS_LOG_UNCOND("Delay =" <<iter->second.delaySum);
  NS_LOG_UNCOND("Jitter =" <<iter->second.jitterSum);
  NS_LOG_UNCOND("Throughput =" <<iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024<<"Kbps");

  f << "Flow ID:" << iter->first << std::endl <<
    "Src Addr: " <<t.sourceAddress << std::endl << "Dst Addr: "<< t.destinationAddress << std::endl <<
    "Sent Packets=" <<iter->second.txPackets<< std::endl <<
    "Received Packets =" <<iter->second.rxPackets<< std::endl <<
    "Lost Packets =" <<iter->second.txPackets-iter->second.rxPackets<< std::endl <<
    "Packet delivery ratio =" <<iter->second.rxPackets*100/iter->second.txPackets << "%"<< std::endl <<
    "Packet loss ratio =" << (iter->second.txPackets-iter->second.rxPackets)*100/iter->second.txPackets << "%"<< std::endl <<
    "Delay =" <<iter->second.delaySum<< std::endl <<
    "Jitter =" <<iter->second.jitterSum<< std::endl <<
    "Throughput =" <<iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024<<"Kbps" << std::endl;

  SentPackets = SentPackets +(iter->second.txPackets);
  ReceivedPackets = ReceivedPackets + (iter->second.rxPackets);
  LostPackets = LostPackets + (iter->second.txPackets-iter->second.rxPackets);
  AvgThroughput = AvgThroughput + (iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024);
  Delay = Delay + (iter->second.delaySum);
  Jitter = Jitter + (iter->second.jitterSum);

  

  j = j + 1;

  }

  AvgThroughput = AvgThroughput/j;
  NS_LOG_UNCOND("--------Total Results of the simulation----------"<<std::endl);
  NS_LOG_UNCOND("Total sent packets  =" << SentPackets);
  NS_LOG_UNCOND("Total Received Packets =" << ReceivedPackets);
  NS_LOG_UNCOND("Total Lost Packets =" << LostPackets);
  NS_LOG_UNCOND("Packet Loss ratio =" << ((LostPackets*100)/SentPackets)<< "%");
  NS_LOG_UNCOND("Packet delivery ratio =" << ((ReceivedPackets*100)/SentPackets)<< "%");
  NS_LOG_UNCOND("Average Throughput =" << AvgThroughput<< "Kbps");
  NS_LOG_UNCOND("End to End Delay =" << Delay);
  NS_LOG_UNCOND("End to End Jitter delay =" << Jitter);
  NS_LOG_UNCOND("Total Flod id " << j);
  monitor->SerializeToXmlFile("lteFlow.xml", true, true);

  f << "END"<<std::endl <<
  "Total sent packets  =" << SentPackets << std::endl <<
  "Total Received Packets =" << ReceivedPackets << std::endl <<
  "Total Lost Packets =" << LostPackets << std::endl <<
  "Packet Loss ratio =" << ((LostPackets*100)/SentPackets)<< "%" << std::endl <<
  "Packet delivery ratio =" << ((ReceivedPackets*100)/SentPackets)<< "%" << std::endl <<
  "Average Throughput =" << AvgThroughput<< "Kbps" << std::endl <<
  "End to End Delay =" << Delay<< std::endl <<
  "End to End Jitter delay =" << Jitter<< std::endl <<
  "Total Flod id " << j;

  }

  f.close();
  Simulator::Destroy();
  return 0;
}

