/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/netanim-module.h"
/*
*
*  *      *             !                 E          E = echo server
*  n7     n6            n0 -----n1 n2 n3 n4          ! = Accesspoint
*                           p2p  ==========            * = wifiStaNodes , n5 is wifiStaNode[1]
*                                  csma1
*                                             
*                       *                          
*                      n5 -----n1 n2 n3 n4          
*                          p2p  ==========
*                                  csma2
*
*
*/

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ThirdScriptExample");

int 
main (int argc, char *argv[])
{
  bool verbose = true;
  uint32_t nCsma = 3;
  uint32_t nWifi = 3;
  bool tracing = true;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
  cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  cmd.Parse (argc,argv);

  // The underlying restriction of 18 is due to the grid position
  // allocator's configuration; the grid layout will exceed the
  // bounding box if more than 18 nodes are provided.
  if (nWifi > 18)
    {
      std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
      return 1;
    }

  if (verbose)
    {
      LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
      LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

  NodeContainer p2pNodes,p2pNodes2;
  p2pNodes.Create (2);
  p2pNodes2.Create (2);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  NetDeviceContainer p2pDevices,p2pDevices2;
  p2pDevices = pointToPoint.Install (p2pNodes);
  p2pDevices2 = pointToPoint.Install(p2pNodes2);

  NodeContainer csmaNodes,csmaNodes2;
  csmaNodes.Add (p2pNodes.Get (1));
  csmaNodes.Create (nCsma);

  csmaNodes2.Add (p2pNodes2.Get (1));
  csmaNodes2.Create (nCsma);

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));

  NetDeviceContainer csmaDevices,csmaDevices2;
  csmaDevices = csma.Install (csmaNodes);
  csmaDevices2 = csma.Install (csmaNodes2);

  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (nWifi);
  NodeContainer wifiApNode = p2pNodes.Get (0);
  wifiApNode.Add(p2pNodes2.Get(0));
  
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy;
  phy.SetChannel (channel.Create ());

  WifiHelper wifi;
  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

  WifiMacHelper mac;
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);

  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);

  

  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-50, 50, -50, 50)));
  mobility.Install (wifiStaNodes);


  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);

  InternetStackHelper stack;
  stack.Install (csmaNodes);
  stack.Install (csmaNodes2);
  stack.Install (wifiApNode);
  stack.Install (wifiStaNodes);

  Ipv4AddressHelper address;

  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfaces;
  p2pInterfaces = address.Assign (p2pDevices);

  address.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces;
  csmaInterfaces = address.Assign (csmaDevices);

  address.SetBase ("10.1.3.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces2;
  csmaInterfaces2 = address.Assign(csmaDevices2);

  address.SetBase ("10.1.4.0", "255.255.255.0");
  address.Assign (staDevices);
  address.Assign (apDevices);

  UdpEchoServerHelper echoServer (9);

    // These comments are when nCsma and nWifi as 3,
    // the echo server is at 10.1.2.4
  ApplicationContainer serverApps = echoServer.Install (csmaNodes.Get (nCsma));
  serverApps.Start (Seconds (1.0));
  serverApps.Stop (Seconds (10.0));

  UdpEchoClientHelper echoClient (csmaInterfaces.GetAddress (nCsma), 9);
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024));


    // wifiStaNode[0] is 10.1.4.1
  ApplicationContainer clientApps = 
    echoClient.Install (wifiStaNodes.Get (0));
  clientApps.Start (Seconds (2.0));
  clientApps.Stop (Seconds (10.0));

    //csmaNodes[0] is 10.1.2.1
  ApplicationContainer clientApps2 = 
    echoClient.Install (csmaNodes.Get(0));
  clientApps2.Start (Seconds (3.0));
  clientApps2.Stop (Seconds (10.0));

  // making another connection to pass data from csmanode2 to wifiStaNode
  UdpEchoClientHelper passData (csmaInterfaces.GetAddress (nCsma), 9);
  passData.SetAttribute ("MaxPackets", UintegerValue (1));
  passData.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  passData.SetAttribute ("PacketSize", UintegerValue (2048));

  //csmaNodes2.Get(0) = wifiApNode.Get(0);

    //wifiApNode.get(0) is 10.1.1.1
  ApplicationContainer clientApps3 = 
    passData.Install (csmaNodes2.Get(0));
  clientApps3.Start (Seconds (4.0));
  clientApps3.Stop (Seconds (10.0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
    // create pcap files for debugging
      phy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
      pointToPoint.EnablePcapAll ("wifiTest");
      phy.EnablePcap ("wifiTest", apDevices.Get (0));
      csma.EnablePcap ("wifiTest", csmaDevices.Get (0), true);

//  AnimationInterface anim ("wifiTest.xml");
//   Ptr<Node> n = wifiStaNodes.Get(0);
//   Ptr<Node> n1 = wifiStaNodes.Get(1);
//   Ptr<Node> n2 = wifiStaNodes.Get(2);
//   Ptr<Node> n3 = csmaNodes.Get(0);
//   Ptr<Node> n4 = csmaNodes.Get(1);
//   Ptr<Node> n5 = csmaNodes.Get(2);
//   Ptr<Node> n6 = csmaNodes.Get(3);
//   Ptr<Node> n7 = csmaNodes2.Get(0);
//   Ptr<Node> n8 = csmaNodes2.Get(1);
//   Ptr<Node> n9 = csmaNodes2.Get(2);
//   Ptr<Node> n10 = csmaNodes2.Get(3);
//   Ptr<Node> n11 = p2pNodes.Get(0);
//   Ptr<Node> n12 = p2pNodes.Get(1);
//   Ptr<Node> n13 = p2pNodes2.Get(0);
//   // this is ugly but ill clean things up once it is working
//   anim.SetConstantPosition (n1, 10, 20);
//   anim.SetConstantPosition (n2, 15, 20);
//   anim.SetConstantPosition (n3, 20, 20);
//   anim.SetConstantPosition (n4, 25, 20);
//   anim.SetConstantPosition (n5, 30, 20);
//   anim.SetConstantPosition (n6, 40, 20);
//   anim.SetConstantPosition (n7, 45, 20);
//   anim.SetConstantPosition (n8, 50, 20);
//   anim.SetConstantPosition (n9, 55, 20);
//   anim.SetConstantPosition (n10, 60,20);
//   anim.SetConstantPosition (n11, 10,30);
//   anim.SetConstantPosition (n12, 15,30);
//   anim.SetConstantPosition (n13, 20,30);
  



  

  Simulator::Stop (Seconds (10.0));
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
