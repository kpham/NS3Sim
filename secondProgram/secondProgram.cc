#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"

//net anim include
#include "ns3/netanim-module.h"

//flow monitor includes
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"

// planned topology
//
//            p2p
//  n0  n1 -------- n2  n3
//  ======         =======
//   csma            csma
//
// two pairs of csma networks connected by a p2p

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Kevin's Second Program :)");

int
main (int argc, char *argv[])
{
  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
  //output in console

  NodeContainer p2pPair;
  p2pPair.Create(2);
  // [n1, n2] will connect via p2p

  NodeContainer pair1;
  pair1.Create (1);
  pair1.Add(p2pPair.Get(0));
  //[n0, n1]

  NodeContainer pair2;
  pair2.Add(p2pPair.Get(1));
  pair2.Create (1);
  //[n2, n3]

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));
  //settings for p2p connection

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
  //settings for csma connections
  
  NetDeviceContainer p2pDevices;
  p2pDevices = pointToPoint.Install(p2pPair);

  NetDeviceContainer csmaPair1;
  csmaPair1 = csma.Install (pair1);

  NetDeviceContainer csmaPair2;
  csmaPair2 = csma.Install(pair2);

  InternetStackHelper stack;
  stack.Install(pair1);
  stack.Install(pair2);
  //stack.Install(p2pPair.Get(0));
  /* this broke my code, im keeping it here as a reminder
  Later on I call assign on adrdess to p2p, and i guess a p2p
  connection cant have both the internetStack and an IPV4
  */

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterface1;
  csmaInterface1 = address.Assign (csmaPair1);

  address.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterface;
  p2pInterface = address.Assign(p2pDevices);

  address.SetBase ("10.1.3.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterface2;
  csmaInterface2 = address.Assign (csmaPair2);

  UdpEchoServerHelper echoServer (9);
  // create a UDP echo server on port 9

  //make n2 the echoServer 
  ApplicationContainer serverApps = echoServer.Install(p2pPair.Get (1));
  
  serverApps.Start (Seconds (1.0));
  serverApps.Stop (Seconds (10.0));

  // echo client will send a packet to the address at csmaInterface1[1]
  // which should be node n2
  UdpEchoClientHelper echoClient (p2pInterface.GetAddress (1), 9);
  
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

  //assign node n1 to send something from 2-3 seconds
  ApplicationContainer clientApps = echoClient.Install (pair1.Get (1));
  clientApps.Start (Seconds (2.0));
  clientApps.Stop (Seconds (3.0));

  //assign node n2 to send something from 3-4 seconds
  ApplicationContainer clientApps2 = echoClient.Install (pair2.Get (1));
  clientApps2.Start (Seconds (3.0));
  clientApps2.Stop (Seconds (4.0));
  
  //this generates the pcap files
  csma.EnablePcapAll ("secondProgram");
  csma.EnablePcap ("secondProgram", p2pDevices.Get(0), true);

 // net anim stuff
  AnimationInterface anim ("secondProgram.xml");
  Ptr<Node> n0 = pair1.Get(0);
  Ptr<Node> n1 = pair1.Get(1);
  Ptr<Node> n2 = pair2.Get(0);
  Ptr<Node> n3 = pair2.Get(1);
  anim.SetConstantPosition(n0,10,40); // called n2 in netanim?
  anim.SetConstantPosition(n1,20,40); // called n0
  anim.SetConstantPosition(n2,30,40); // called n1
  anim.SetConstantPosition(n3,40,40); // called n3
  //each node is 1 unit away from the next

  // I can not get this stuff to work for some reason, console log just gets stuck
  //FlowMonitorHelper flowHelp;
  //Ptr<FlowMonitor> flow = flowHelp.InstallAll();


  Simulator::Run ();
  Simulator::Destroy ();

  //flow->SerializeToXmlFile("flowSecondProgram.xml",true,true);

  return 0;
}
